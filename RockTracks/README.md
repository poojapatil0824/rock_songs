# Rock Tracks App Using MVVM Design Pattern

-   I have used **Swift 5** and **Xcode 11.2** to develop this app
-   This is **iOS Mobile** app and deployment target is supported for **iOS 9.3**
-   I have implemented **MVVM** design pattern for the project architecture 
-   The Rock Tracks app has two screens,
    1.    Track List
    2.    Track Details
-   I was not sure about the date format so I have assumed it to be **dd-MMM-yyyy**
-   To load the images faster next time, I have implemeneted **Image Caching**  
-   I have also attached screenshots and application architecture diagram
