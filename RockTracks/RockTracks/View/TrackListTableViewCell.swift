//
//  TrackListTableViewCell.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

import UIKit

class TrackListTableViewCell: UITableViewCell {
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var trackImageView: UIImageView!
    
    // MARK: set function to set the image to imageview
       func set(track: TrackDetails) {
           ImageCacheService.getImage(withURL: track.artworkUrl100!) { image in
            if (image != nil) {
               self.trackImageView.image = image
            }
           }
       }
}
