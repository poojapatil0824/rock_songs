//
//  TrackListWebService.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class TrackListWebService {
    
}

extension TrackListWebService: TrackListWebServiceProtocol {
    func fetchTrackListDataFromServer(completion: @escaping (_ track : [TrackDetails]?, Error?) -> Void) {
        let url = URL(string: "https://itunes.apple.com/search?term=rock&media=music")
        
        guard let downloadURL = url else {return}
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("\(error.debugDescription)")
                return
            }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(TrackList.self, from: data)
                let trackDataset = response.results
                return completion(trackDataset, nil)
                
            }catch {
                print("Something went wrong", error)
            }
        }.resume()
    }
}
