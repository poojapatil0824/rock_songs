//
//  TrackListWebServiceProtocol.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

protocol TrackListWebServiceProtocol {
    func fetchTrackListDataFromServer(completion: @escaping (_ track : [TrackDetails]?, _ error: Error?) -> Void)
}
