//
//  TrackListViewModel.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class TrackListViewModel {
    
    private let trackListWebService: TrackListWebService
    
    init(trackListWebService: TrackListWebService) {
        self.trackListWebService = trackListWebService
    }
    
    public func getTrackListDataFromService(completion: @escaping (_ track : [TrackDetails]?, _ error: Error?) -> Void) {
        trackListWebService.fetchTrackListDataFromServer() { (track, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let track = track  else { return }
            return completion(track, nil)
        }
    }
    public func getFormattedReleaseDate(releaseDate: String?) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MMM-yyyy"
        if let releaseDate = releaseDate {
            
            if let formattedDate = dateFormatterGet.date(from: releaseDate) {
                return dateFormatterPrint.string(from: formattedDate)
            }
            else {
                return "Incorrect Date Format"
            }
        }else {
            return "Invalid Date"
        }
    }
    public func getFormattedDuration (milliseconds: Int?) -> String {
        if let milliseconds = milliseconds {
            let millisecRemainder = milliseconds % 1000
            let seconds = (milliseconds - millisecRemainder) / 1000
            let secondsRemainder = seconds % 60
            let minutes = (seconds - secondsRemainder) / 60
            return "\(minutes):\(secondsRemainder)"
        }
        else {
            return "Invalid Duration"
        }
    }
    
    public func getFormattedPrice (price : Double, currency: String?) -> String {
        var priceString:String = ""
        if let currency = currency {
            priceString = String(price) + " " + currency
        }
        return priceString
    }
    
    
}
