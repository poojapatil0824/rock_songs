//
//  TrackList.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

struct TrackList: Codable {
    let results: [TrackDetails]
}
struct TrackDetails {
    let trackName: String?
    let artistName: String?
    let trackPrice: Double?
    let artworkUrl60: URL?
    let artworkUrl100: URL?
    let trackTimeMillis: Int?
    let currency: String?
    let releaseDate: String?
    let trackViewUrl: String?
}

extension TrackDetails: Codable {
    enum CodingKeys: String, CodingKey {
        case trackName
        case artistName
        case trackPrice
        case artworkUrl60
        case artworkUrl100
        case trackTimeMillis
        case currency
        case releaseDate
        case trackViewUrl
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        trackName = try values.decodeIfPresent(String.self, forKey: .trackName)
        artistName = try values.decodeIfPresent(String.self, forKey: .artistName)
        trackPrice = try values.decodeIfPresent(Double.self, forKey: .trackPrice)
        artworkUrl60 = try values.decodeIfPresent(URL.self, forKey: .artworkUrl60)
        artworkUrl100 = try values.decodeIfPresent(URL.self, forKey: .artworkUrl100)
        trackTimeMillis = try values.decodeIfPresent(Int.self, forKey: .trackTimeMillis)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)
        trackViewUrl = try values.decodeIfPresent(String.self, forKey: .trackViewUrl)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(trackName, forKey: .trackName)
        try container.encodeIfPresent(artistName, forKey: .artistName)
        try container.encodeIfPresent(trackPrice, forKey: .trackPrice)
        try container.encodeIfPresent(artworkUrl60, forKey: .artworkUrl60)
        try container.encodeIfPresent(artworkUrl100, forKey: .artworkUrl100)
        try container.encodeIfPresent(trackTimeMillis, forKey: .trackTimeMillis)
        try container.encodeIfPresent(currency, forKey: .currency)
        try container.encodeIfPresent(releaseDate, forKey: .releaseDate)
        try container.encodeIfPresent(trackViewUrl, forKey: .trackViewUrl)
    }
}
