//
//  TrackListViewController.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class TrackListViewController: UIViewController {
    
    @IBOutlet weak var trackListTableView: UITableView!
    
    private var listOfTrackArray = [TrackDetails]()
    
    private let trackListWebService = TrackListWebService()
    
    var viewModel = TrackListViewModel(trackListWebService: TrackListWebService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        trackListTableView.tableFooterView = UIView(frame: .zero)
        loadTrackList()
    }
    
    private func loadTrackList() {
        viewModel.getTrackListDataFromService() { (track, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let track = track  else { return }
            self.listOfTrackArray = track
            DispatchQueue.main.async {
                self.trackListTableView.reloadData()
            }
        }
    }
}

extension TrackListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  listOfTrackArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackListCell") as! TrackListTableViewCell
        cell.set(track: listOfTrackArray[indexPath.row])
        
        cell.trackNameLabel?.text = listOfTrackArray[indexPath.row].trackName
        cell.artistLabel?.text = listOfTrackArray[indexPath.row].artistName
        cell.selectionStyle = .none
        let price = listOfTrackArray[indexPath.row].trackPrice
        let currency = listOfTrackArray[indexPath.row].currency
        if let price = price {
            cell.priceLabel?.text = viewModel.getFormattedPrice(price: price, currency: currency)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! TrackListTableViewCell
        
        let trackDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "TrackListDetailVC") as! DetailSelectedTrackViewController
        trackDetailViewController.trackDetailedImage = currentCell.trackImageView.image!
        trackDetailViewController.trackDetails = listOfTrackArray[indexPath.row]
        self.navigationController?.pushViewController(trackDetailViewController, animated: true)
    }
}


