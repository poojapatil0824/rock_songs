//
//  DetailSelectedTrackViewController.swift
//  RockTracks
//
//  Created by Pooja Awati on 15/11/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class DetailSelectedTrackViewController: UIViewController {
    var viewModel = TrackListViewModel(trackListWebService: TrackListWebService())
    
    
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var trackPriceLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var detailImageView: UIImageView!
    
    @IBAction func MoreDetailsAction(_ sender: Any) {
        if let url = NSURL(string: (trackDetails?.trackViewUrl)!){
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    var trackListViewController = TrackListViewController()
    var trackDetailedImage = UIImage()
    var trackDetails: TrackDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailImageView.image = trackDetailedImage
        trackNameLabel.text = trackDetails?.trackName
        artistLabel.text = trackDetails?.artistName
        let price = trackDetails?.trackPrice
        let currency = trackDetails?.currency
        if let price = price {
            trackPriceLabel.text = viewModel.getFormattedPrice(price: price, currency: currency)
        }
        
        let trackDuration = trackDetails?.trackTimeMillis
        let trackReleaseDate = trackDetails?.releaseDate
        releaseDate.text = viewModel.getFormattedReleaseDate(releaseDate: trackReleaseDate)
        durationLabel.text = viewModel.getFormattedDuration(milliseconds: trackDuration)
    }
}
